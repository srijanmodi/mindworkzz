function parse(val) {
	var result = "NA",
		tmp = [];
	location.search
		//.replace ( "?", "" ) 
		// this is better, there might be a question mark inside
		.substr(1)
		.split("&")
		.forEach(function(item) {
			tmp = item.split("=");
			if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
		});
	return result;
}

(function () {
		var utm_source = parse("utm_source");
        var utm_campaign = parse("utm_campaign");
        var utm_term = parse("utm_term");
        var utm_medium = parse("utm_medium");

		try
		{
			var ls = window['localStorage'];

			data=ls.getItem("__clutm");
			jsdata=JSON.parse(data);

			utm_source = jsdata.last.source;
			utm_campaign = jsdata.last.campaign;
			utm_medium = jsdata.last.medium;
		}
		catch(err) {
		}


        if (!((utm_source == null) || (utm_source == '') || (utm_source == 'NA') || typeof utm_source === 'undefined')) {
            document.clForm.utm_source.value = utm_source;
        }
        if (!((utm_campaign == null) || (utm_campaign == '') || (utm_campaign == 'NA') || typeof utm_campaign === 'undefined')) {
            document.clForm.utm_campaign.value = utm_campaign;
        }
        if (!((utm_term == null) || (utm_term == '') || (utm_term == 'NA') || typeof utm_term === 'undefined')) {
            document.clForm.utm_term.value = utm_term;
        }
        if (!((utm_medium == null) || (utm_medium == '') || (utm_medium == 'NA') || typeof utm_medium === 'undefined')) {
            document.clForm.utm_medium.value = utm_medium;
        }
})();